<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
    parent::__construct();
	$this->load->helper('url'); 
	$this->load->database();
	$this->load->model('admin_model');
  	}

  	public function index(){
  	     	
  	}
    public function reg(){
	    if($this->session->userdata('logged_in')==TRUE and $this->session->userdata('tipo')=='1'){
	      $this->load->view('templete/header');//
	      $this->load->view('admin/form_reg');
	      $this->load->view('templete/footer');
	  	}else{
	  		redirect('Inicio/modulo');
	  	}
    }
  	public function add(){
      if($this->input->post('submit')){
        $add=$this->admin_model->add(
          $this->input->post('nombre'),
          $this->input->post('apellidoP'),
          $this->input->post('apellidoM'),
          $this->input->post('user'),
          md5($this->input->post('pass'))
        );
        if($add==true){
        $this->session->set_flashdata('msg','El administrador fue creado existosamente');
          redirect('Inicio');
      }else{
        $this->session->set_flashdata('msg','No se pudo crear el administrador o ya hay uno con ese  usuario');
          redirect('Inicio');
        
         }
      }
  	}
    public function ver_admin(){
      $ver=$this->admin_model->ver($this->session->userdata('id'));
      if($ver->num_rows() > 0){
        $user  = $ver->row_array();
        $data= array(
          'nombre'=>$user['nombre'],
          'apellidoP'=>$user['apellidoP'],
          'apellidoM'=>$user['apellidoM'],
          'usuario'=>$user['usuario']
        );
        if($this->session->userdata('logged_in')){
            $this->load->view('templete/header');//
            $this->load->view('admin/most_adm',$data);
            $this->load->view('templete/footer');
        }else{
          redirect('Inicio');
        }
      }
    }

    public function mod(){
      $ver=$this->admin_model->ver($this->session->userdata('id'));
      if($ver->num_rows() > 0){
        $user  = $ver->row_array();
        $data= array(
          'nombre'=>$user['nombre'],
          'apellidoP'=>$user['apellidoP'],
          'apellidoM'=>$user['apellidoM'],
          'usuario'=>$user['usuario']
        );
        if($this->session->userdata('logged_in')){
            $this->load->view('templete/header');//
            $this->load->view('admin/form_mod',$data);
            $this->load->view('templete/footer');
        }else{
          redirect('Inicio');
        }
      }
    }
    public function modificar(){
      if($this->input->post('submit')){
        $mod=$this->admin_model->mod(
          $this->session->userdata('id'),
          $this->input->post('nombre'),
          $this->input->post('apellidoP'),
          $this->input->post('apellidoM'),
          $this->input->post('user')
          
        );
        if($mod==true){
        $this->session->set_flashdata('msg','El usuario fue modificado existosamente');
          redirect('Admin/ver_admin');
      }else{
        $this->session->set_flashdata('msg','No se pudo moficar el usuario ');
          redirect('Admin/ver_admin');
         }
      }
    }
    public function eli($id){
      $elim=$this->admin_model->eliminar($id);
      if($elim==true){
        $data['msg']='El administrador fue elimino existosamente';
        redirect('Inicio',$dat);
      }else {
        $this->$this->session->set_flashdata('msg','El administrador no se puedo eliminar');
        redirect('Admin/ver_admin');
      }
    }
    public function ver_admis(){
      $data['result']=$this->admin_model->list_adm();
      $this->load->view('templete/header');//
      $this->load->view('admin/admin',$data);
      $this->load->view('templete/footer');
    }

    public function ver_user(){
      $data['result']=$this->admin_model->list_usr();
      $this->load->view('templete/header');//
      $this->load->view('admin/users',$data);
      $this->load->view('templete/footer');
    }
}