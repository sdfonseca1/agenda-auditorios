<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditorio extends CI_Controller {
	function __construct(){
    parent::__construct();
	$this->load->helper('url'); 
	$this->load->model('auditorio_model');
  	}

  	public function index(){
  		if($this->session->userdata('logged_in')){
		   if($this->session->userdata('tipo')==0){
	  		$this->load->view('templete/header');
		 	$this->load->view('auditorios/index');
		 	$this->load->view('templete/footer');
		    }elseif($this->session->userdata('tipo')==1){
			$this->load->view('templete/header');
			$this->load->view('auditorios/index2');
			$this->load->view('templete/footer');
		     }
		 }else{
		 	redirect('Inicio');
		 }
	 }
	public function verAud($id){
	 	if($this->session->userdata('logged_in')){
		 	if($id==118){
		 		$data['result']=$this->auditorio_model->view(1)->result();
		 		if (!empty($data['result'])) {
		 			foreach ($data['result'] as $key =>  $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/auditorio118',$data);
				 	$this->load->view('templete/footer');
		 		}else{
		 			$data['data'] = null;
		 			$this->load->view('templete/header');
		 			$this->load->view('auditorios/user/auditorio118',$data);
		 			$this->load->view('templete/footer');
		 		}
		 		
		 	}
		 	if($id==119){
		 		$data['result']=$this->auditorio_model->view(2)->result();
		 		if (!empty($data['result'])) {
		 			foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/auditorio119',$data);
				 	$this->load->view('templete/footer');
		 		}else{
		 			$data['data'] = null;
		 			$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/auditorio119',$data);
				 	$this->load->view('templete/footer');
		 		}
		 		
		 	}
		 	if($id==120){
		 		$data['result']=$this->auditorio_model->view(3)->result();
		 		if (!empty($data['result'])) {
			 		foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/auditorio120',$data);
				 	$this->load->view('templete/footer');
				 }else{
				 	$data['data'] = null;
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/auditorio120',$data);
				 	$this->load->view('templete/footer');
				 }
		 	}
		 	if($id==121){
		 		$data['result']=$this->auditorio_model->view(4)->result();
		 		if (!empty($data['result'])) {
			 		foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/salaMul',$data);
				 	$this->load->view('templete/footer');
				}else{
					$data['data'] = null;
					$this->load->view('templete/header');
				 	$this->load->view('auditorios/user/salaMul',$data);
				 	$this->load->view('templete/footer');
				}

		 	}
		}else{
		 	redirect('Inicio');
		}
	 }

	public function verAud_adm($id){
	 	if($this->session->userdata('logged_in')){
		 	if($id==118){
		 		$data['result']=$this->auditorio_model->view(1)->result();
		 		if (!empty($data['result'])) {
			 		foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;

			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio118',$data);
				 	$this->load->view('templete/footer');
				}else{
					$data['data'] = null;
					$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio118',$data);
				 	$this->load->view('templete/footer');
				}
		 	}
		 	if($id==119){
		 		$data['result']=$this->auditorio_model->view(2)->result();
		 		// var_dump($data['result']);
		 		if (!empty($data['result'])) {
		 			foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio119',$data);
				 	$this->load->view('templete/footer');
		 		}else{
		 			$data['data'] = null;
		 			$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio119',$data);
				 	$this->load->view('templete/footer');
		 		}
		 		
		 	}
		 	if($id==120){
		 		$data['result']=$this->auditorio_model->view(3)->result();
		 		if (!empty($data['result'])) {
		 			foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio120',$data);
				 	$this->load->view('templete/footer');
		 		}else{
		 			$data['data'] = null;
		 			$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/auditorio120',$data);
				 	$this->load->view('templete/footer');
		 		}
		 		
		 	}
		 	if($id==121){
		 		$data['result']=$this->auditorio_model->view(4)->result();
		 		if (!empty($data['result'])) {
			 		foreach ($data['result'] as $key => $value) {
			 			$data['data'][$key]['title']=$value->evento.', Solicitado por: '.$value->nombre.' '.$value->apellidoP;
			 			$data['data'][$key]['description']=$value->nombre.' '.$value->apellidoP.' '.$value->apellidoM.': '.$value->descripcion;
			 			$data['data'][$key]['start']=$value->fecha_ini;
			 			$data['data'][$key]['end']=$value->fecha_fin;
			 			$data['data'][$key]['backgroundColor']=$value->bg_color;
			 		}
				 	$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/salaMul',$data);
				 	$this->load->view('templete/footer');
			 	}else{
			 		$data['data'] = null;
			 		$this->load->view('templete/header');
				 	$this->load->view('auditorios/admin/salaMul',$data);
				 	$this->load->view('templete/footer');
			 	}
		 		
		 	}
		}else{
		 	redirect('Inicio');
		}
	} 

	public function add_Aud(){
		if($this->input->post('submit')){
			if($this->input->post('fechaFin')==null){
				$add=$this->auditorio_model->add_Aud(
				$this->input->post('evento'),
				$this->input->post('descripcion'),
				($this->input->post('fechaIni').'T'.$this->input->post('horaIni').':00'),
				($this->input->post('fechaIni').'T'.$this->input->post('horaFin').':00'),
				$this->input->post('idAudi'),
				$this->session->userdata('id'),
				'#E67E22',
				0);
					
			}else{
				$add=$this->auditorio_model->add_Aud(
				$this->input->post('evento'),
				$this->input->post('descripcion'),
				($this->input->post('fechaIni').'T'.$this->input->post('horaIni').':00'),
				($this->input->post('fechaFin').'T'.$this->input->post('horaFin').':00'),
				$this->input->post('idAudi'),
				$this->session->userdata('id'),
				'#E67E22',
				0);
			}
			if($add==true){
				$this->session->set_flashdata('msg','El evento se ha creado existosamente');
				if($this->input->post('idAudi')==1){
					redirect('Auditorio/verAud/118');
				}elseif($this->input->post('idAudi')==2){
					redirect('Auditorio/verAud/119');
				}elseif($this->input->post('idAudi')==3){
					redirect('Auditorio/verAud/120');
				}elseif($this->input->post('idAudi')==4){
					redirect('Auditorio/verAud/121');
				}
			}else{
				$this->session->set_flashdata('msg','El evento no se pudo crear');
				if($this->input->post('idAudi')==1){
					redirect('Auditorio/verAud/118');
				}elseif($this->input->post('idAudi')==2){
					redirect('Auditorio/verAud/119');
				}elseif($this->input->post('idAudi')==3){
					redirect('Auditorio/verAud/120');
				}elseif($this->input->post('idAudi')==4){
					redirect('Auditorio/verAud/121');
				}
			}
		}
	}

	public function update_acep($id){
		
		if($cons=$this->auditorio_model->update_acep($id)->row_array()){
			if($cons['id_auditorio']==1){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/118');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/118');
			}
			if($cons['id_auditorio']==2){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/119');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/119');
			}
			if($cons['id_auditorio']==3){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/120');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/120');
			}
			if($cons['id_auditorio']==4){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/121');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/121');
			}
		}
	}
	public function update_rech($id){
		if($cons=$this->auditorio_model->update_rech($id)->row_array()){
			if($cons['id_auditorio']==1){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/118');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/118');
			}
			if($cons['id_auditorio']==2){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/119');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/119');
			}
			if($cons['id_auditorio']==3){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/120');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/120');
			}
			if($cons['id_auditorio']==4){
				$this->session->set_flashdata('msg','Se actulizo correctamente el evento');
				redirect('Auditorio/verAud_adm/121');
			}else{
				$this->session->set_flashdata('msg','No se pudo actulizar correctamente el evento');
				redirect('Auditorio/verAud_adm/121');
			}
		}
	}
 }

 ?>
