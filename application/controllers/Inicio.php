<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->model('inicio_model');
	$this->load->helper('url'); 
	$this->load->database(); 
  	}

	public function index(){
    
		  $this->load->view('index');
    
	}
	public function auth(){
	$this->load->helper('url'); 
    $user    = $this->input->post('usuario',TRUE);
    $password = $this->input->post('pass',TRUE);
    if($this->input->post('loggin')){
      $validate = $this->inicio_model->validate($user,md5($password));
      $tipo='0';
    }elseif($this->input->post('loggin_adm')){
      $validate = $this->inicio_model->validate_adm($user,md5($password));
      $tipo='1';
    }
    if($validate->num_rows() > 0){
        $data  = $validate->row_array();
        $id=$data['id'];
        $name  = $data['nombre'].' '.$data['apellidoP'].' '.$data['apellidoM'];
        $user = $data['usuario'];
        
        $sesdata = array(
            'id' =>$id,
            'nombre'  => $name,
            'tipo'=>$tipo,
            'user'     => $user,
            'logged_in' => TRUE
        );
        $this->session->set_userdata($sesdata);
       redirect('Inicio/modulo');
    }else{
        $this->session->set_flashdata('msg','Usuario o password erroneos');
        redirect('inicio');
    }
  }

 public function modulo(){ 
    if($this->session->userdata('logged_in')){
        $this->load->view('templete/header');
 	      $this->load->view('modulos');
        $this->load->view('templete/footer');
      }else {
        redirect('Inicio');
      }
 }

 public function logout(){
      $this->session->sess_destroy();
      $sesdata = array(
            'logged_in' => FALSE
        );
      $this->session->set_userdata($sesdata);
      redirect('inicio');
  }
 

}
