<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	function __construct(){
    parent::__construct();
	$this->load->helper('url'); 
	$this->load->database();
	$this->load->model('usuario_model');
  	}
  	public function index(){
  	     	
  	}
    public function reg(){
      $this->load->view('templete/header_sin');
      $this->load->view('usuario/form_reg');
      $this->load->view('templete/footer');
    }
  	public function add(){
      if($this->input->post('submit')){
        $add=$this->usuario_model->add(
          $this->input->post('nombre'),
          $this->input->post('apellidoP'),
          $this->input->post('apellidoM'),
          $this->input->post('email'),
          $this->input->post('user'),
          md5($this->input->post('pass')),
          $this->input->post('tipo')
        );
        if($add==true){
        $this->session->set_flashdata('msg','El usuario fue creado existosamente');
          redirect('Inicio');
      }else{
        $this->session->set_flashdata('msg','No se pudo crear el usuario o ya hay un usuario');
          redirect('Inicio');
        
         }
      }
  	}
    public function ver_user(){
      $ver=$this->usuario_model->ver($this->session->userdata('id'));
      if($ver->num_rows() > 0){
        $user  = $ver->row_array();
        $data= array(
          'nombre'=>$user['nombre'],
          'apellidoP'=>$user['apellidoP'],
          'apellidoM'=>$user['apellidoM'],
          'usuario'=>$user['usuario'],
          'email'=>$user['email'],
          'tipo'=>$user['tipo']
        );
        if($this->session->userdata('logged_in')){
            $this->load->view('templete/header');
            $this->load->view('usuario/most_user',$data);
            $this->load->view('templete/footer');
        }else{
          redirect('Inicio');
        }
      }
    }
    public function mod(){
      $ver=$this->usuario_model->ver($this->session->userdata('id'));
      if($ver->num_rows() > 0){
        $user  = $ver->row_array();
        $data= array(
          'nombre'=>$user['nombre'],
          'apellidoP'=>$user['apellidoP'],
          'apellidoM'=>$user['apellidoM'],
          'email'=>$user['email'],
          'usuario'=>$user['usuario'],
          'tipo'=>$user['tipo'],
        );
        if($this->session->userdata('logged_in')){
            $this->load->view('templete/header');
            $this->load->view('usuario/form_mod',$data);
            $this->load->view('templete/footer');
        }else{
          redirect('Inicio');
        }
      }
    }
    public function modificar(){
      if($this->input->post('submit')){
        $mod=$this->usuario_model->mod(
          $this->session->userdata('id'),
          $this->input->post('nombre'),
          $this->input->post('apellidoP'),
          $this->input->post('apellidoM'),
          $this->input->post('email'),
          $this->input->post('user'),
          $this->input->post('tipo')
        );
        if($mod==true){
        $this->session->set_flashdata('msg','El usuario fue modificado existosamente');
          redirect('Usuario/ver_user');
      }else{
        $this->session->set_flashdata('msg','No se pudo moficar el usuario ');
          redirect('Usuario/ver_user');
         }
      }
    }
    public function eli(){
      $elim=$this->usuario_model->eliminar($this->session->userdata('id'));
      if($elim==true){
        $data['msg']='El usuario se elimino existosamente';
        redirect('Inicio/logout');
      }else {
        $this->$this->session->set_flashdata('msg','El usuario no se puedo eliminar');
        redirect('Usuario/ver_user');
      }
    }
}