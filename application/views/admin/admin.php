<script type="text/javascript">
	$(document).ready(function() {
	    $('#administrador').DataTable();
	} );
</script>
<div class="container">
	<p class="display-4 text-center text-success my-3 ">Administradores registrados</p>
	<div class="row">
		<table class="offset-md-3 table table-responsive text-center">
			<thead>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Usuario</th>
			</thead>
			<tbody>
				<?php
					foreach($result as $fila){
				?>
				<tr>
					<td>
						<?=$fila->nombre?>
					</td>
					<td>
						<?=$fila->apellidoP?>
					</td>
					<td>
						<?=$fila->apellidoM?>
					</td>
					<td>
						<?=$fila->usuario?>
					</td>
					<td>
						<a class="" style="color: white;" href="<?=base_url()?>index.php/Admin/eliminar/<?=$fila->id?>">
							<img src="../../assets/icons/eliminar.png">
						</a>
					</td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
		<div class="offset-6 mb-5">
			<a class="mb-5" href="<?=base_url()?>index.php/Admin/reg">
				<img src="../../assets/icons/agregar_64.png" title="Añadir nuevo administrador">
			</a>
		</div>
		
	</div>
</div>