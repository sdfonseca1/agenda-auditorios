<div class="container text-center">
  <div class="">
        <p class="display-4 my-5 text-success">Modificar Datos Administrador</p>
        <form class="needs-validation row container" validation method="post" action="<?=base_url()?>index.php/admin/modificar">
            <div class="col-sm-12 col-md-4">
                <label for="Nombre" class="text-success font-weight-bold">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="<?=$nombre?>" required>
                <div class="invalid-feedback">
                  EL nombre es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellidoP" name="apellidoP" placeholder="" value="<?=$apellidoP?>" required>
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
              </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Materno </label>
                <input type="text" class="form-control" id="apellidoM" name="apellidoM" placeholder="" value="<?=$apellidoM?>" required="">
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="user" class="text-success font-weight-bold">Usuario</label>
                <input type="text" class="form-control" id="user" name="user" placeholder="Username" value="<?=$usuario?>" required>
                <div class="invalid-feedback" style="width: 100%;">
                    El usuario es requerido.
                </div>
            </div>
            <div class="offset-md-8 my-3"></div>
            <input class="btn btn-success float-left offset-md-3 col-sm-12 col-md-2 my-2 mb-5" type="submit" name="submit" value="Guardar">
            <a class="btn btn-secondary text-white float-righ offset-md-2 col-sm-12 my-2 col-md-2 mb-5" href="<?= base_url()?>index.php/admin/ver_admin">Regresar</a>
          </form>
    </div>
</div>