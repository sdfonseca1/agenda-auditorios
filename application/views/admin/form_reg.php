<div class="container">
    <p class="display-4 text-center text-success my-3">Añadir un nuevo Adminsitrador</p>
    <form class="was-validation" method="post" action="<?=base_url()?>index.php/admin/add">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <label for="Nombre" class="text-success font-weight-bold">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" required>
                <div class="invalid-feedback">
                  EL nombre es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellidoP" name="apellidoP" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
              </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Materno </label>
                <input type="text" class="form-control" id="apellidoM" name="apellidoM" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="user" class="text-success font-weight-bold">Usuario</label>
                <input type="text" class="form-control" id="user" name="user" placeholder="Username" required>
                <div class="invalid-feedback" style="width: 100%;">
                    El usuario es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                  <label for="user" class="text-success font-weight-bold">Contraseña</label>
                  <div class="input-group">
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" pattern=".{8,}" title="8 caracteres minimo" required>
                    <div class="invalid-feedback" style="width: 100%;">
                      La contraseña es requerido.
                    </div>
                  </div>
            </div>
            <input class="btn btn-success float-left offset-md-3 col-sm-12 col-md-2 mb-3 my-5" type="submit" name="submit" value="Guardar">
            <a class="btn btn-secondary text-white float-righ offset-md-2 col-sm-12 col-md-2 mb-3 my-5" href="<?= base_url()?>index.php/admin/ver_admis">Regresar</a>
        </div>
    </form>
</div>