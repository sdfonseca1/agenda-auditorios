<div class="container">
	<div class="card-body">
	    <div class="card">
	    	<p class="card-title text-center my-2 display-4 text-success">Mis Datos</p>
	    	<div class="card-body"></div>
	    	<div class="row text-center">
	    		<div class="col-sm-12 col-md-4">
	    			<p class="font-weight-bold text-success">Nombre:</p>
	    			<p><?=$nombre?></p>
	    		</div>
	    		<div class="col-sm-12 col-md-4">
	    			<p class="font-weight-bold text-success">Apellido Paterno:</p><p><?=$apellidoP?></p>
	    		</div>
				<div class="col-sm-12 col-md-4">
	    			<p class="font-weight-bold text-success">Apellido Materno:</p>
	    			<p><?=$apellidoM?></p>
	    		</div>
	    		<div class="col-sm-12 col-md-4">
	    			<p class="font-weight-bold text-success">Usuario:</p>
	    			<p><?=$usuario?></p>
	    		</div>
	    	</div>
		    <div class="text-center mb-3">
		    	<a href="<?=base_url()?>index.php/admin/mod" class=" col- btn btn-success text-white font-weight-bold">Modificar</a>
				<a href="<?=base_url()?>index.php/admin/eli" class=" col-0 btn btn-danger text-white font-weight-bold" >Eliminar</a>
		    </div>
		</div>         
	</div>
</div>