<script type="text/javascript">
	$(document).ready(function() {
	    $('#administrador').DataTable();
	} );
</script>
<div class="container">
	<p class="display-4 text-center text-success my-3 ">Usuarios registrados</p>
	<div class="row">
		<table class="offset-md-2 table table-responsive text-center">
			<thead>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Email</th>
				<th>Usuario</th>
				<th>Tipo</th>
			</thead>
			<tbody>
				<?php
					foreach($result as $fila){
				?>
				<tr>
					<td>
						<?=$fila->nombre?>
					</td>
					<td>
						<?=$fila->apellidoP?>
					</td>
					<td>
						<?=$fila->apellidoM?>
					</td>
					<td>
						<?=$fila->email?>
					</td>
					<td>
						<?=$fila->usuario?>
					</td>
					<td>
						<?=$fila->tipo?>
					</td>
					<td>
						<a style="color: white;" href="<?=base_url()?>index.php/usuario/eli/<?=$fila->id?>">
							<img src="../../assets/icons/eliminar.png">
						</a>
					</td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>