<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var eventos = <?php echo json_encode($data) ?>;
    var calendar = new FullCalendar.Calendar(calendarEl, {
      locale: 'es',	
      lang: 'es',
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek'
      },
      defaultDate: new Date(),
      //businessHours: true, // display business hours
        buttonText: {
        today: 'Hoy',
        month: 'Mes',
        week : 'Semana'
      },
      events: eventos
        
    });

    calendar.render();
  });
	$(document).ready(function() {
      $('#auditorio').DataTable();
  } );
</script>

<div class="container">
    <div class="container text-center">
        <p class="display-4 text-success my-3">Auditorio 119</p>
        <span style="color: green;"><?php echo $this->session->flashdata('msg');?></span>
    </div>
    <div class="container row text-center">
        <!-- Tabla de solicitudes -->
        <div class="col-sm-12 col-md-6 container align-content-center">
            <h2 class="mb-3 text-success font-weight-bold text-center" >Eventos Registrados</h2>
            <?php
                if ($data == null){
                    echo "<div class='container'><p class='alert alert-warning text-center'>No hay eventos todavía registrados</p></div>";
                }else{
            ?>
            <table class="table table-responsive" id="auditorio">
                <thead>
                    <tr>
                        <th>Evento</th>
                        <th>Solicitante</th>
                        <th>Descripcion</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($result as $value){
                            if($value->estado!=1){
                    ?>
                        <tr>
                            <td><?=$value->evento?></td>
                            <td><?=$value->nombre." ".$value->apellidoP." ".$value->apellidoM?></td>
                            <td><?=$value->descripcion?></td>
                            <td>
                                <a style="color: white;" name="aceptar" href="<?=base_url()?>index.php/Auditorio/update_acep/<?=$value->id?>"><img src="../../../assets/icons/aceptar.png" alt=""></a>
                            </td>
                            <td>
                                <a style="color: white;" name="rechazar" href="<?=base_url()?>index.php/Auditorio/update_rech/<?=$value->id?>"><img src="../../../assets/icons/eliminar.png" alt=""></a>
                            </td>
                        </tr>
                    <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
            <?php
                }
            ?>
        </div>
        <!-- Calendar -->
        <div class="col-sm-12 col-md-6 container" id="calendar"></div>      
    </div>
    <div class="my-5"></div>  
</div>
