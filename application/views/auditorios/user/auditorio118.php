<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var eventos = <?php echo json_encode($data) ?>;
    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'es',	
        lang: 'es',
        plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek'
        },
        defaultDate: new Date(),
        buttonText: {
            today: 'Hoy',
            month: 'Mes',
            week : 'Semana'
        },
        events: eventos 
    });

    calendar.render();
});
</script>
<!-- <div class="my-3"></div> -->
<div class="container">
    <div class="container text-center">
        <p class="display-4 text-success my-3">Auditorio 118</p>
        <span style="color: green;"><?php echo $this->session->flashdata('msg');?></span>
    </div>
    <div class="container row">
        <!-- Formulario de registro de evento -->
        <div class="col-sm-12 col-md-6">
            <h2 class="mb-3 text-success font-weight-bold my-0 text-center" >Registro de Evento</h2>
            <form class="needs-validation text-center" validation  method="post" action="<?=base_url()?>index.php/Auditorio/add_Aud">
                <input type="hidden" name="idAudi" id="IdAudi" value="1">
                <div class="container col-sm-12">
                    <div class="form-group">
                        <label for="Nombre" class="text-success font-weight-bold">Evento</label>
                        <input type="text" class="form-control" id="Evento" name="evento" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            Se requiere un titulo de evento
                        </div>
                    </div>
                </div>
                <div class="container row">
                    <div class="col-6">
                        <label for="FechaIni" class="text-success font-weight-bold">Fecha de Inicio</label>
                        <input type="date" class="form-control" id="FechaIni" name="fechaIni" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            Se requiere una fecha de inicio de evento
                        </div>
                    </div>
                    <div class="col-6">
                        <label for="FechaFin" class="text-success font-weight-bold">Fecha de Fin</label>
                        <input type="date" class="form-control" id="FechaFin" name="fechaFin"  >
                        <div class="invalid-feedback">
                            Se requiere una fecha de fin de evento
                        </div>
                    </div>
                </div>
                <div class="container row">
                    <div class="col-6">
                        <label for="HoraIni" class="text-success font-weight-bold">Hora de Inicio</label>
                        <input type="time" class="form-control" id="HoraIni" name="horaIni" min="07:00" max="18:00" required>
                        <div class="invalid-feedback">
                            Se requiere una hora de inicio de evento
                        </div>
                    </div>
                    <div class="col-6">
                        <label for="HoraIni" class="text-success font-weight-bold">Hora de Fin</span> </label>
                        <input type="time" class="form-control" id="HoraIni" name="horaFin" min="08:00" max="21:00" required>
                        <div class="invalid-feedback">
                            Se requiere una hora de fin de evento
                        </div>
                    </div>
                </div>
                <div class="container row">
                    <div class="col-12">
                        <label for="Descripcion" class="text-success font-weight-bold">Descripcion <span class="text-muted">(Opcional)</span> </label>
                        <textarea class="form-control" id="Descripcion" name="descripcion" rows="3"></textarea>
                    </div>
                 </div>
                <div class="container row my-3">
                    <input class="btn btn-success offset-3 col-3" type="submit" name="submit" value="Guardar">
                    <a class="btn btn-secondary text-white offset-1 col-3" href="<?= base_url()?>index.php/Auditorio">Regresar</a>
                </div>
            </form>            
        </div>
        <!-- Calendar -->
        <div class="col-sm-12 col-md-6" id="calendar"></div>
    </div>
</div>

