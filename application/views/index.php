<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="bg">
        <form action="<?php echo base_url();?>index.php/inicio/auth" class="box" method="post">
            <h1>Login</h1>
            <span style="color: white;"><?php echo $this->session->flashdata('msg');?></span>
            <!--<span style="color: white;"><?php echo $msg;?></span>-->
            <input type="text" name="usuario" placeholder="Usuario">
            <input type="password" name="pass" placeholder="Contraseña">
            <input type="submit" name="loggin" class="loggin" value="Ingresar">
            <div class="row">
                <input type="submit"  class="col btn btn-default" style="color: white;" name="loggin_adm" value="Login_admin">
                <a href="<?=base_url()?>index.php/usuario/reg" style="color: white;" class="col btn btn-default loggin_adm">Registrarse</a>
            </div>
        </form>
    </div>
</body>
</html>