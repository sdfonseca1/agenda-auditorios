
<div class="modulos">
    <div class="contenedor-modulos">
        <h1>Módulos del sistema</h1>
        <div class="mod">
            <a href="moduloExpedientes.html">
                <div class="servicio">
                    <i class="fas fa-folder-open"></i>
                    <h2>Expedientes Docentes</h2>
                    <h3 class="desc">Crear, Borrar, Modificar y Buscar los expedientes docentes almacenados en la base de datos</h3>
                </div>
            </a>

            <div class="servicio">
                    <i class="fas fa-user-check" ></i> 
                    <h2>Listas Docentes</h2>
                    <h3 class="desc">[Descripción de las funcionalidades del módulo]</h3>
            </div>

            <div class="servicio">
                    <i class="fas fa-archive"></i>
                    <h2>Archivo</h2>
                    <h3 class="desc">[Descripción de las funcionalidades del módulo]</h3>
            </div>
            <a href="<?= base_url()?>index.php/auditorio">
                <div class="servicio">
                    <i class="fas fa-chalkboard-teacher"></i>
                    <h2>Control de auditorios</h2>
                    <h3 class="desc">Se llevara acabo la Agendacionde auditorios para eventos</h3>
                </div>
            </a>
        </div>
        
    </div>
</div>