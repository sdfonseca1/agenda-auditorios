<style>
    footer {
      background: #a8924a;
      color: #fff;
      padding-top: 10px;
       /* position: absolute;
      bottom: 0;
      width: 100%;
      height: auto;
 */    }

    footer a {
      color: #fff;
    }

    footer a:hover {
      color: #fff;
    }

    footer h3 {
     color: white;
      letter-spacing: 1px;
      margin: 30px 0 20px;
    }

    footer .three-column {
     overflow: hidden;
    }

    footer .three-column li{
     width: 33.3333%;
      float: left;
      padding: 5px 0;
    }

    footer .socila-list {
      overflow: hidden;
      margin: 20px 0 10px;
    }

    footer .socila-list li {
      float: left;
      margin-right: 3px;
      opacity: 0.7;
      overflow: hidden;
      border-radius: 50%;
      transition: all 0.3s ease-in-out;
    }

    footer .socila-list li:hover {
      opacity: 1;
    }

    footer .img-thumbnail {
      background: rgba(0, 0, 0, 0.2);
      border: 1px solid #444;
      margin-bottom: 5px;
    }

    footer .copyright {
      padding: 15px 0;
      background: #333;
      margin-top: 20px;
      font-size: 15px;
    }

    footer .copyright span {
      color: #0894d1;
    }
</style>
<footer>
    <div class="container" id="fott">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <h3>Mapa del Sitio</h3>
                <ul class="list-unstyled three-column">
                    <li><a class="text-white" href="<?=base_url()?>index.php/inicio/modulo">Inicio</a></li>
                    <li><a class="text-white" href="<?=base_url()?>index.php/auditorio">Auditorios</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6">
                <h3>Contacto</h3>
                <div class="media">
                    <div class="media-body">
                        <p style="margin:0% 0;">Facultad de Ingeniería UAEM</p>
                        <p style="margin:0% 0;">Cerro de Coatepec S/N</p>
                        <p style="margin:0% 0;">Ciudad Universitaria C.P. 50100.</p>
                        <p style="margin:0% 0;">Toluca, Estado de México</p>
                        <p style="margin:0% 0;">Tels.:(+52 722) 214 08 55 y 214 05 34</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <h3>Redes Sociales</h3>
                <a href="https://www.facebook.com/FIUAEM/">
                    <img src="./facebook.png" alt="">
                </a>
                
            </div>
        </div>
    </div>
<!--   <div class="copyright text-center">
    Copyright &copy; 2017 <span>Your Template Name</span>
  </div> -->
</footer>
