<!DOCTYPE html>
<html lang="es"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Control de Auditorios</title>
    <link rel="stylesheet" href='<?php echo base_url(); ?>assets/packages/core/main.css'/>
	<link rel='stylesheet' href='<?php echo base_url(); ?>assets/packages/daygrid/main.css' />
	<link  rel='stylesheet' href='<?php echo base_url(); ?>assets/packages/timegrid/main.css'  />
	<link  rel='stylesheet' href='<?php echo base_url(); ?>assets/packages/list/main.css' />
	<script src='<?php echo base_url(); ?>assets/packages/core/main.js'></script>
	<script src='<?php echo base_url(); ?>assets/packages/interaction/main.js'></script>
	<script src='<?php echo base_url(); ?>assets/packages/daygrid/main.js'></script>
	<script src='<?php echo base_url(); ?>assets/packages/timegrid/main.js'></script>
	<script src='<?php echo base_url(); ?>assets/packages/list/main.js'></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">
	<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css">
</head>
<body class="bg-light">
<header>



<!-- Menu de usuario normal-->
<?php if ($this->session->userdata('tipo')==0) {
	?>
	<nav class="navbar navbar-expand-lg navbar-dark info-color" style="background: #15441f;">
		<div class="navbar-brand">
    		<img src="../../assets/img/fingenieria.png" width="130" class="d-inline-block align-top" alt="">
    		<p class="navbar-brand">Control de Auditorios</p>
  		</div>
	    
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	        <ul class="navbar-nav ml-auto">
	            <li class="nav-item">
	                <a class="nav-link" style="color: white" href="<?=base_url()?>index.php/inicio/modulo">Inicio</a>
	            </li>
	            <li class="nav-item dropdown">
	                <a class="nav-link dropdown-toggle" style="color: white" href="#" id="navbardrop" data-toggle="dropdown">
	                    Auditorios
	                </a>
	                <div class="dropdown-menu" style="background: #15441f">
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud/118">Auditorio 118</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud/119">Auditorio 119</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud/120">Auditorio 120</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud/121">Sala Multimedia</a>
	                </div>
	            </li>
	            <li class="nav-item dropdown mr-md-2">
	                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
	                <i class="fas fa-user" style="color: white"></i> <?=$this->session->userdata('nombre') ?> </a>
	                <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4" style="background: #15441f">
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/usuario/ver_user" >Mi cuenta</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/inicio/logout" >Cerrar Sesión</a>
	                </div>
	            </li>
	        </ul>
	    </div>
	</nav>
<?php }

elseif($this->session->userdata('tipo')==1){
?>
	<!-- Administrador -->
	<nav class="navbar navbar-expand-lg navbar-dark info-color" style="background: #15441f;">
	    <div class="navbar-brand">
    		<img src="../../assets/img/fingenieria.png" width="130" class="d-inline-block align-top" alt="">
    		<p class="navbar-brand">Control de Auditorios</p>
  		</div>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	        <ul class="navbar-nav ml-auto">
	            <li class="nav-item">
	                <a class="nav-link" style="color: white" href="<?=base_url()?>index.php/inicio/modulo">Inicio</a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" style="color: white" href="<?=base_url()?>index.php/admin/ver_user">Usuarios</a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" style="color: white" href="<?=base_url()?>index.php/admin/ver_admis">Administradores</a>
	            </li>
	            <li class="nav-item dropdown">
	                <a class="nav-link dropdown-toggle" style="color: white" href="#" id="navbardrop" data-toggle="dropdown">
	                    Eventos
	                </a>
	                <div class="dropdown-menu" style="background: #15441f">
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud_adm/118">Auditorio 118</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud_adm/119">Auditorio 119</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud_adm/120">Auditorio 120</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/Auditorio/verAud_adm/121">Sala Multimedia</a>
	                </div>
	            </li>
	            <li class="nav-item dropdown float-right">
	                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
	                <i class="fas fa-user" style="color: white"></i> <?=$this->session->userdata('nombre') ?> </a>
	                <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4" style="background: #15441f">
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/admin/ver_admin" >Mi cuenta</a>
	                    <a class="dropdown-item text-white" href="<?=base_url()?>index.php/inicio/logout" >Cerrar Sesión</a>
	                </div>
	            </li>
	        </ul>
	    </div>
	</nav>
<?php
	}
?>
</header>

