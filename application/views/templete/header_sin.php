<!DOCTYPE html>
<html lang="es"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Control de Auditorios</title>
	 <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css">
</head>
<body class="bg-light">
<header>
	<!-- <img src="<?=base_url()?>assets/img/bannerFI.png" class="img-fluid img-thumbnai banner1" id="banner1" name="banner1">	 -->
	<nav class="navbar navbar-expand-lg navbar-dark info-color" style="background: #15441f;">
		<div class="navbar-brand">
    		<img src="../../assets/img/fingenieria.png" width="130" class="d-inline-block align-top" alt="">
    		<p class="navbar-brand">Control de Auditorios</p>
  		</div>
	    
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	        <ul class="navbar-nav ml-auto">
	            <li class="nav-item">
	                <a class="nav-link" style="color: white" href="<?=base_url()?>index.php/inicio/modulo">Inicio</a>
	            </li>
	        </ul>
	    </div>
	</nav>
</header>