
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Control de Auditorios</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Inicio<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Usuarios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Admnistradores</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" style="color: white" href="#" id="navbardrop" data-toggle="dropdown">
                    Auditorios
                </a>
                <div class="dropdown-menu" style="background: #15441f">
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/Auditorio/verAud/118" style="color: #927913">Auditorio 118</a>
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/Auditorio/verAud/119" style="color: #927913">Auditorio 119</a>
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/Auditorio/verAud/120" style="color: #927913">Auditorio 120</a>
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/Auditorio/verAud/121" style="color: #927913">Sala Multimedia</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
                <i class="fas fa-user" style="color: white"></i> <?//=$this->session->userdata('nombre') ?> </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/usuario/ver_user" >Mi cuenta</a>
                    <a class="dropdown-item" href="<?//=base_url()?>index.php/inicio/logout" >Log out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>