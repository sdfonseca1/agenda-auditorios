<div class="container text-center">
  <div class="">
        <p class="display-4 my-5 text-success">Modificar Datos Usuario</p>
        <form class="needs-validation row container" validation method="post" action="<?=base_url()?>index.php/usuario/modificar">
            <div class="col-sm-12 col-md-4">
                <label for="Nombre" class="text-success font-weight-bold">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="<?=$nombre?>" required>
                <div class="invalid-feedback">
                  EL nombre es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellidoP" name="apellidoP" placeholder="" value="<?=$apellidoP?>" required>
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
              </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Materno </label>
                <input type="text" class="form-control" id="apellidoM" name="apellidoM" placeholder="" value="<?=$apellidoM?>" required="">
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Email</label>
                <input type="email" class="form-control" id="Email" name="email" placeholder="ejemplo@example.com" value="<?=$email?>" required>
                <div class="invalid-feedback">
                    Su email es requerido
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="user" class="text-success font-weight-bold">Usuario</label>
                <input type="text" class="form-control" id="user" name="user" placeholder="Username" value="<?=$usuario?>" required>
                <div class="invalid-feedback" style="width: 100%;">
                    El usuario es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4 mb-3">
                <label for="tipo" class="text-success font-weight-bold">Tipo</label>
                <select class="custom-select d-block w-100" id="tipo" name="tipo" required>
                    <?php if ($tipo=="Alumno") {?>
                    <option value="Alumno">Alumno</option>
                    <option value="Profesor">Profesor</option>
                    <?php }elseif($ipo=='Profesor') {?>
                    <option value="Profesor">Profesor</option>
                    <option value="Alumno">Alumno</option>
                    <?php }else{?>
                    <option value=""></option>
                    <option value="Alumno">Alumno</option>
                    <option value="Profesor">Profesor</option>
                    <?php } ?> 
                </select>
                <div class="invalid-feedback">
                    Selecionar un tipo de usuario.
                </div>
            </div>
            <input class="btn btn-success float-left offset-md-3 col-sm-12 col-md-2 mb-3" type="submit" name="submit" value="Guardar">
            <a class="btn btn-secondary text-white float-righ offset-md-2 col-sm-12 col-md-2 mb-3" href="<?= base_url()?>index.php/usuario/ver_user">Regresar</a>
          </form>
    </div>
</div>