<div class="container text-center">
	<div class="">
        <p class="display-4 my-5 text-success">Registro Usuario</p>
        <form class="needs-validation row container" validation method="post" action="<?=base_url()?>index.php/usuario/add">
            <div class="col-sm-12 col-md-4">
                <label for="Nombre" class="text-success font-weight-bold">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" required>
                <div class="invalid-feedback">
                  EL nombre es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Paterno</label>
                <input type="text" class="form-control" id="apellidoP" name="apellidoP" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
              </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Apellido Materno </label>
                <input type="text" class="form-control" id="apellidoM" name="apellidoM" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Su apellido es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="ApellidoP" class="text-success font-weight-bold">Email</label>
                <input type="email" class="form-control" id="Email" name="email" placeholder="ejemplo@example.com" value="" required>
                <div class="invalid-feedback">
                    Su email es requerido
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="user" class="text-success font-weight-bold">Usuario</label>
                <input type="text" class="form-control" id="user" name="user" placeholder="Username" required>
                <div class="invalid-feedback" style="width: 100%;">
                    El usuario es requerido.
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                  <label for="user" class="text-success font-weight-bold">Contraseña</label>
                  <div class="input-group">
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" pattern=".{8,}" title="8 caracteres minimo" required>
                    <div class="invalid-feedback" style="width: 100%;">
                      La contraseña es requerido.
                    </div>
                  </div>
            </div>
            <div class="col-sm-12 offset-md-4 col-md-4 mb-3">
                <label for="tipo" class="text-success font-weight-bold">Tipo</label>
                <select class="custom-select d-block w-100" id="tipo" name="tipo" required>
                    <option value="">Elija una opción</option>
                    <option value="Alumno">Alumno</option>
                    <option value="Profesor">Profesor</option>
                </select>
                <div class="invalid-feedback">
                    Selecionar un tipo de usuario.
                </div>
            </div>
            <input class="btn btn-primary float-left offset-md-3 col-sm-12 col-md-2 mb-3" type="submit" name="submit" value="Guardar">
            <a class="btn btn-success text-white float-righ offset-md-2 col-sm-12 col-md-2 mb-3" href="<?= base_url()?>">Regresar</a>
          </form>
    </div>
</div>