-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: dbAgendaAuditorios
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidoP` varchar(45) DEFAULT NULL,
  `apellidoM` varchar(45) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (2,'admin','admin','admin','admin','81dc9bdb52d04dc20036dbd8313ed055'),(3,'david','fonseca','hrdz','sdfonseca1','sdfonseca1');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditorios`
--

DROP TABLE IF EXISTS `auditorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditorios`
--

LOCK TABLES `auditorios` WRITE;
/*!40000 ALTER TABLE `auditorios` DISABLE KEYS */;
INSERT INTO `auditorios` VALUES (1,'Auditorio 118'),(2,'Auditorio 119'),(3,'Auditorio 120'),(4,'Sala Multimedia');
/*!40000 ALTER TABLE `auditorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `fecha_ini` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `id_auditorio` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `bg_color` varchar(10) DEFAULT NULL,
  `estado` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eventos_auditorios` (`id_auditorio`),
  KEY `fk_eventos_usuarios1` (`id_usuario`),
  CONSTRAINT `fk_eventos_auditorios` FOREIGN KEY (`id_auditorio`) REFERENCES `auditorios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_eventos_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES (8,'ini','','2000-10-05 00:00:00','2000-10-05 00:00:00',1,10,'#FF0000',2),(9,'ini','','2000-10-05 00:00:00','2000-10-05 00:00:00',2,10,'',0),(10,'ini','','2000-10-05 00:00:00','2000-10-05 00:00:00',3,10,'',0),(11,'ini','','2000-10-05 00:00:00','2000-10-05 00:00:00',4,10,'#FF0000',2),(12,'Exposiones','Exposiciones de prueba','2019-11-11 07:00:00','2019-11-11 09:00:00',1,12,'#00FF00',1),(13,'Prueba','Esto es una prueba','2019-12-03 08:00:00','2019-12-03 12:30:00',1,12,'#FF0000',2),(14,'REUINION DE PROFESORES','REUNIÓN CON PROFESORES DE NUEVO INGRESO','2019-11-20 12:00:00','2019-11-20 13:00:00',1,13,'#00FF00',1),(15,'Junta de profesores','Nuevo evento','2019-11-27 10:00:00','2019-11-27 12:30:00',4,11,'#E67E22',0),(16,'Cine','','2020-02-17 08:00:00','2020-02-17 08:00:00',1,15,'#E67E22',0),(17,'Conferencia','','2020-02-17 08:00:00','2020-02-17 08:00:00',1,15,'#E67E22',0),(18,'Nacimiento de Cristo','Nacimiento de Cristo','0050-12-25 08:00:00','0050-12-25 08:00:00',1,16,'#E67E22',0);
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidoP` varchar(45) DEFAULT NULL,
  `apellidoM` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (10,'Felipe','Alvarez','Sanchez','fel.a.s52fe@gmail.com','user1','25d55ad283aa400af464c76d713c07ad','Alumno'),(11,'Narvi','Mendoza','Barragan','narvi.mendoza@hotmail.com','narvimen','25d55ad283aa400af464c76d713c07ad','Alumno'),(12,'Yao','Amador','Reynoso','yaoar@live.com.mx','Cadmio','25d55ad283aa400af464c76d713c07ad','Alumno'),(13,'CINTIA','GONZALEZ','MIRELES','cgonzalezmi@uaemex.mx','cgonzalez','25d55ad283aa400af464c76d713c07ad','Profesor'),(14,'Esmeralda','Romero','','eromero_@uaemex.mx','ESME','a2bd839e1f3b7048589560c5a01a2231','Profesor'),(15,'juanito','perez','no que es opcional, lo exige en editar usuari','a@a.com','juanito','0f7e44a922df352c05c5f73cb40ba115','Profesor'),(16,'juanito','perez','','ab@a.com','juanito2','25d55ad283aa400af464c76d713c07ad','Profesor'),(18,'David','Fonseca','Hernández','sdfonseca1@outlook.com','sdfonseca1','sdfonseca1','Alumno');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-10 21:55:31
